# MeteorApp
This is an example of Meteor Application on angular2-now with
- angular babel
- angular material design,
- material design iconic font
- angulat-templates
- angular formly types defined,
- angular ui router,
- upper camel case, a.k.a. PascalCase: ThisIsAnExample. thisIsNotAnExample,
- necolas normalize.css https://github.com/necolas/normalize.css/
- Sass for Meteor

# Install
- meteor npm install --save angular angular-meteor angular-ui-router normalize.css
- npm install --save angular2-now normalize.css
- meteor remove ecmascript blaze-html-templates
- meteor add
    modules
    angular-templates
    pbastowski:angular-babel
    angularui:angular-ui-router
    angular:angular-material
    angular:angular-messages
    formly:angular-formly
    wieldo:api-check
    aldeed:collection2
    ixdi:material-design-iconic-font
    fourseven:scss
